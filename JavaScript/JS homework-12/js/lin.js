let timer;
let left = 0;
timerID = null;
timerID = autoSlider();


function autoSlider() {
    timer = setTimeout(function slide() {
        let polosa = document.getElementsByClassName('polosa')[0];
        left = left - 150;
        if (left < -450) {
            left = 0;
        }


        polosa.style.left = left + 'px';

        timerID = setTimeout(slide, 10000);
    }, 0);

    return timer;
}

let startButton = document.getElementsByClassName('start-button')[0];
startButton.addEventListener('click', () => {
    timerID = autoSlider();
});


let stopButton = document.getElementsByClassName('stop-button')[0];
stopButton.addEventListener('click', () => {
    clearTimeout(timerID);
});
