document.addEventListener('click', function (event) {
    if (event.target.classList.contains('tabs-title')) {
        let currentActiveLi = document.querySelector('.li.active');
        currentActiveLi.classList.remove('active');
        let newActive = document.querySelector(`.li.${event.target.classList[1]}`);
        newActive.classList.add('active');
    }
});


